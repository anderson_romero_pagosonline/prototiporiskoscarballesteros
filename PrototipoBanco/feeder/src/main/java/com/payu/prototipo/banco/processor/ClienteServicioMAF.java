package com.payu.prototipo.banco.processor;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.URL;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

import com.payu.integracion.maf.HuellaComplexType;
import com.payu.integracion.maf.InformacionPagoComplexType;
import com.payu.integracion.maf.UsuarioComplexType;
import com.payu.integracion.maf.ValidarPago;
import com.payu.integracion.maf.respuesta.Respuesta;

/**
 * Clase que implementa el cliente al servicio del modulo antifraudes.
 * @author  <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
public class ClienteServicioMAF {

	/**
	 * Objero Logger que implementa el rastreo para la clase
	 */
	private static final Logger logger = Logger.getLogger(ClienteServicioMAF.class.getName());
		
	/**
	 * Url en la que se encuentra la wsdl del servicio de maf.
	 */
	private String urlServicioMaf;
	/**
	 * Url del enpoint del servicio de maf.
	 */
	private String urlEndpointMaf;
	
	/**
	 * Metodo que se encarga de invocar el servicio del modulo antifraudes.
	 * @param valorTran valor del la transacción a verificar.
	 * @return Objeto con la respuesta del servicio del modulo antifraudes.
	 * @throws Exception Excepcion lanzada por el servicio del modulo antifraudes.
	 */
	public Respuesta invocarServicioMaf(Double valorTran)throws Exception {
		
		URL url = new URL(urlServicioMaf);
		QName serviceName = new QName("http://ws.maf.pagosonline.com/","ModuloAntifraudeWebserviceImplService");
		QName portName = new QName("http://ws.maf.pagosonline.com/","ModuloAntifraudeWebserviceImplPort");
		
		Service service = Service.create(serviceName);
		
		service.addPort(portName, SOAPBinding.SOAP11HTTP_BINDING, urlEndpointMaf);
		
		Dispatch<SOAPMessage> dispatch = service.createDispatch(portName, SOAPMessage.class, Service.Mode.MESSAGE);
	
		MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
		
		SOAPMessage request = mf.createMessage();
        SOAPHeader header = request.getSOAPHeader();
        SOAPBody body = request.getSOAPBody();
        
        QName payloadName = new QName("http://ws.maf.pagosonline.com/", "preValidar", "ns3");
        SOAPBodyElement payload = body.addBodyElement(payloadName);
        
        SOAPElement value = payload.addChildElement("arg0");
        value.addTextNode("7");
        
        SOAPElement value1 = payload.addChildElement("arg1");
        value1.addTextNode("test.tech@pagosonline.com");
        
        SOAPElement value2 = payload.addChildElement("arg2");
        value2.addTextNode("123456");
		
        
        ValidarPago validar = armarXML(valorTran);
        String xml = parseXml(validar);
        
        SOAPElement value3 = payload.addChildElement("arg3");
        value3.addTextNode("<![CDATA[" + xml + "]]>");       
        
        ByteArrayOutputStream soapin = new ByteArrayOutputStream();
        request.writeTo(soapin);
        logger.info(soapin.toString()); 
        
        SOAPMessage reply = dispatch.invoke(request);        
        
        Node nodo = (Node) reply.getSOAPBody().getChildNodes().item(0);
        nodo = (Node) nodo.getChildNodes().item(0);
        
        System.out.println(nodo.getValue());
        
        JAXBContext jc = JAXBContext.newInstance(Respuesta.class);
        Unmarshaller u = jc.createUnmarshaller();
        Respuesta resp = (Respuesta) u.unmarshal( new StreamSource( new StringReader( nodo.getValue() ) ) );
        
        logger.info("Resp " + resp.getMensajeError() + " " + resp.getFecha());
        
		return resp;
	}
	
	/**
	 * Metodo encargado de transaformar el objeto de solicitud hacia el modulo antifraudes a un xml.
	 * @param pago Objeto con la representación de la solicitud al modulo antifraudes.
	 * @return Xml a enviar al modulo antifraudes.
	 */
	private String parseXml(ValidarPago pago) {
		
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(ValidarPago.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(pago, bos);
		} catch (JAXBException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
		}
		
		String xml = new String(bos.toByteArray());
		logger.info("XML " + xml);
		return xml;
	}
	
	/**
	 * Metodo encargado de construir el objeto ValidarPago con la información de la transacción a validar.
	 * @param valorTransac valor de la transaccion a validar.
	 * @return objeto con la solicitud a enviar al modulo antifraudes.
	 */
	private ValidarPago armarXML(Double valorTransac) {
		DatatypeFactory factoria= null;
		try {
			factoria = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
		}
		
		ValidarPago pago = new ValidarPago();
		pago.setTransaccionId("123456789");
		GregorianCalendar gCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
		pago.setFechaCreacion(factoria.newXMLGregorianCalendar(gCalendar));
		pago.setPrueba(false);
		pago.setPais("CO");
		pago.setOrigen("1100");
		
		UsuarioComplexType usuarioVendedor = new UsuarioComplexType();
		usuarioVendedor.setId("1");
		usuarioVendedor.setTipoVendedor(2l);
		usuarioVendedor.setRubro("10");
		usuarioVendedor.setCodigoClasificacionComercio("AGV01");
		
		pago.setUsuarioVendedor(usuarioVendedor);
		
		UsuarioComplexType usuarioComprador = new UsuarioComplexType();
		usuarioComprador.setId("765456");
		HuellaComplexType huella = new HuellaComplexType();
		huella.setDireccionIp("127.0.0.1");
		huella.setFirmaDispositivo("asd89ahs98hf9a8");
		huella.setUserAgent("Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US)");
		usuarioComprador.setHuella(huella);
		
		pago.setUsuarioComprador(usuarioComprador);
		
		pago.setDescripcion("Compra de muebles para apartamento");
		pago.setValor(BigDecimal.valueOf(valorTransac));
		
		InformacionPagoComplexType infoPago = new InformacionPagoComplexType();
		infoPago.setTipoMedioPago("1");
		infoPago.setMedioPago("10");
		infoPago.setPan("4111111111111111");
		infoPago.setBin("444444");
		infoPago.setNumero("9BBEF19476623CA56C17DA75FD57734DBF82530686043A6E491C6D71BEFE8F6E");
		infoPago.setNumeroVisible("1111");
		infoPago.setMoneda("COP");
		infoPago.setFechaExpiracion("12-15");
		infoPago.setCodigoSeguridad("123");
		infoPago.setCuotas("12");
		
		pago.setInformacionPago(infoPago);
		

		return pago;
	}


	public String getUrlServicioMaf() {
		return urlServicioMaf;
	}


	public void setUrlServicioMaf(String urlServicioMaf) {
		this.urlServicioMaf = urlServicioMaf;
	}


	public String getUrlEndpointMaf() {
		return urlEndpointMaf;
	}


	public void setUrlEndpointMaf(String urlEndpointMaf) {
		this.urlEndpointMaf = urlEndpointMaf;
	}
	
}

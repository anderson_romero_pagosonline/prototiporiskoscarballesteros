package com.payu.prototipo.banco.processor;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openspaces.core.GigaSpace;
import org.openspaces.events.TransactionalEvent;

import com.payu.integracion.maf.respuesta.Respuesta;
import com.payu.prototipo.banco.persistencia.Cuenta;
import com.payu.prototipo.banco.persistencia.Movimiento;
import com.payu.prototipo.banco.persistencia.TipoMovimiento;


/**
 * Procesador de movimientos para el espacio del banco, se encarga de verificar que una cuenta no quede con saldo negativo.
 * @author <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 */
@TransactionalEvent(timeout=0)
public class Processor {	
	/**
	 * Logger utilizado para realizar trazas del procesamiento de información.
	 */
	private Logger logger = Logger.getLogger(Processor.class.getName());
	
	/**
	 * Identificador de aprovacion de una transacción por parte del
	 * Modulo antifraude.
	 */
	private static final int ID_ESTADO_MAF_APROBADO = 3;
	
	
	
	/**
	 * Objeto representado el api de interacción con el espacio del banco.
	 */
	private GigaSpace gigaSpace;
	/**
	 * Cliente del servio web del modulo antifraudes.
	 */
	private ClienteServicioMAF clienteMaf; 
	
    /**
     * Procesa un movimiento y verifica que el saldo de la cuenta no sea negativo, 
     * de igual forma se verifica en el modulo antifraude la transaccion.
     */	
    public Movimiento processData(Movimiento data) {
        logger.info("Movimiento generado"+ data.getId() + "para la cuenta " + data.getCuenta().getNumero());
        
        Cuenta cuenta = gigaSpace.readById(Cuenta.class, data.getCuenta().getNumero());
        
        if(null == cuenta) {
        	logger.info("Cuenta no existe");
        	data.setEstado(Movimiento.ESTADO_ERROR);
        } else {
        	logger.info("Cuenta existe verificando el saldo");
        	
        	BigDecimal saldoActual = cuenta.getSaldo();
    		logger.info("La cuenta tiene saldo " + saldoActual);
    		
    		if(TipoMovimiento.ID_TIPO_MOVIMIENTO_DEBITO == data.getTipoMovimiento().getId()){
    			logger.info("Se realizara un movimiento tipo Debito");
    			
    			BigDecimal nuevoSaldo = saldoActual.add(data.getValor());
    			logger.info("El nuevo saldo sera "+ nuevoSaldo);
    			cuenta.setSaldo(nuevoSaldo);
    			cuenta.getMovimientos().add(data);
    			data.setCuenta(cuenta);
    			data.setEstado(Movimiento.ESTADO_ACEPTADO);
    			gigaSpace.write(cuenta);
    			
    		} else if (TipoMovimiento.ID_TIPO_MOVIMIENTO_CREDITO == data.getTipoMovimiento().getId()) {
    			logger.info("Se realizara un movimiento tipo credito");
    			BigDecimal nuevoSaldo = saldoActual.subtract(data.getValor());
    			logger.info("El nuevo saldo seria " + nuevoSaldo.doubleValue());
    			
    			if(nuevoSaldo.compareTo(BigDecimal.ZERO) == -1) {
    				logger.warning("No se puede realizar el movimiento, el saldo de la cuenta seria menor a 0");
    				data.setEstado(Movimiento.ESTADO_FONDOS_INSUFICIENTES);
    			} else {
    				
    				try {
						Respuesta respuesta =  clienteMaf.invocarServicioMaf(data.getValor().doubleValue());
						int decision = respuesta.getRespuestaMaf().getDecision();
						
						if(decision == ID_ESTADO_MAF_APROBADO) {
							logger.info("Compra aprobada por maf");
							cuenta.setSaldo(nuevoSaldo);
		    				cuenta.getMovimientos().add(data);
		        			data.setCuenta(cuenta);
		        			data.setEstado(Movimiento.ESTADO_ACEPTADO);
		        			gigaSpace.write(cuenta);
						} else {
							logger.info("Compra no aprobada por maf");
							data.setEstado(Movimiento.ESTADO_PENDIENTE);
						}
						
						
					} catch (Exception e) {
						logger.log(Level.SEVERE, e.getMessage());
						data.setEstado(Movimiento.ESTADO_ERROR);
					}
    			}		        	
           }
        }        
        
        return data;
    }

	public GigaSpace getGigaSpace() {
		return gigaSpace;
	}

	public void setGigaSpace(GigaSpace gigaSpace) {
		this.gigaSpace = gigaSpace;
	}

	public ClienteServicioMAF getClienteMaf() {
		return clienteMaf;
	}

	public void setClienteMaf(ClienteServicioMAF clienteMaf) {
		this.clienteMaf = clienteMaf;
	}

}

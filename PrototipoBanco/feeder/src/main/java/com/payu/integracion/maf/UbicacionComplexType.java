//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.02.02 at 05:59:50 PM COT 
//


package com.payu.integracion.maf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ubicacion-ComplexType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ubicacion-ComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigo-iso-pais" type="{https://maf.pagosonline.net/ws}ISO3166"/>
 *         &lt;element name="nombre-pais" type="{https://maf.pagosonline.net/ws}StringLength0to64"/>
 *         &lt;element name="region" type="{https://maf.pagosonline.net/ws}StringLength0to64"/>
 *         &lt;element name="ciudad" type="{https://maf.pagosonline.net/ws}StringLength0to64"/>
 *         &lt;element name="codigo-postal" type="{https://maf.pagosonline.net/ws}StringLength0to16"/>
 *         &lt;element name="latitud" type="{https://maf.pagosonline.net/ws}StringLength0to16"/>
 *         &lt;element name="longitud" type="{https://maf.pagosonline.net/ws}StringLength0to16"/>
 *         &lt;element name="codigo-area-metropolitano" type="{https://maf.pagosonline.net/ws}StringLength0to16"/>
 *         &lt;element name="codigo-area-telefono" type="{https://maf.pagosonline.net/ws}StringLength0to16"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ubicacion-ComplexType", propOrder = {
    "codigoIsoPais",
    "nombrePais",
    "region",
    "ciudad",
    "codigoPostal",
    "latitud",
    "longitud",
    "codigoAreaMetropolitano",
    "codigoAreaTelefono"
})
public class UbicacionComplexType {

    @XmlElement(name = "codigo-iso-pais", required = true)
    protected String codigoIsoPais;
    @XmlElement(name = "nombre-pais", required = true)
    protected String nombrePais;
    @XmlElement(required = true)
    protected String region;
    @XmlElement(required = true)
    protected String ciudad;
    @XmlElement(name = "codigo-postal", required = true)
    protected String codigoPostal;
    @XmlElement(required = true)
    protected String latitud;
    @XmlElement(required = true)
    protected String longitud;
    @XmlElement(name = "codigo-area-metropolitano", required = true)
    protected String codigoAreaMetropolitano;
    @XmlElement(name = "codigo-area-telefono", required = true)
    protected String codigoAreaTelefono;

    /**
     * Gets the value of the codigoIsoPais property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoIsoPais() {
        return codigoIsoPais;
    }

    /**
     * Sets the value of the codigoIsoPais property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoIsoPais(String value) {
        this.codigoIsoPais = value;
    }

    /**
     * Gets the value of the nombrePais property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombrePais() {
        return nombrePais;
    }

    /**
     * Sets the value of the nombrePais property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombrePais(String value) {
        this.nombrePais = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Gets the value of the ciudad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * Sets the value of the ciudad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudad(String value) {
        this.ciudad = value;
    }

    /**
     * Gets the value of the codigoPostal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * Sets the value of the codigoPostal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPostal(String value) {
        this.codigoPostal = value;
    }

    /**
     * Gets the value of the latitud property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLatitud() {
        return latitud;
    }

    /**
     * Sets the value of the latitud property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLatitud(String value) {
        this.latitud = value;
    }

    /**
     * Gets the value of the longitud property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLongitud() {
        return longitud;
    }

    /**
     * Sets the value of the longitud property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLongitud(String value) {
        this.longitud = value;
    }

    /**
     * Gets the value of the codigoAreaMetropolitano property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAreaMetropolitano() {
        return codigoAreaMetropolitano;
    }

    /**
     * Sets the value of the codigoAreaMetropolitano property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAreaMetropolitano(String value) {
        this.codigoAreaMetropolitano = value;
    }

    /**
     * Gets the value of the codigoAreaTelefono property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAreaTelefono() {
        return codigoAreaTelefono;
    }

    /**
     * Sets the value of the codigoAreaTelefono property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAreaTelefono(String value) {
        this.codigoAreaTelefono = value;
    }

}

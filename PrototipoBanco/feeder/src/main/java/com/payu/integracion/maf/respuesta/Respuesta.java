package com.payu.integracion.maf.respuesta;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.payu.integracion.maf.RespuestaMafPrevalidacionComplexType;

/**
 * Clase representado el xml retornado por el modulo antifraudes usando Jaxb.
 * @author  <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
@XmlRootElement(name="respuesta")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder={"fecha","codigoRespuesta","codigoError","mensajeError","respuestaMaf"})
public class Respuesta {
	
	/**
	 * Fecha de la respuesta.
	 */
	@XmlElement(name="fecha")
	@XmlSchemaType(name = "date")
	private Date fecha;
	/**
	 * Codigo de respuesta del modulo antifraudes.
	 */
	@XmlElement(name="codigo-respuesta")
	private int codigoRespuesta;
	/**
	 * Codigo de error retornado por el modulo antifraudes.
	 */
	@XmlElement(name="codigo-error")
	private int codigoError;
	@XmlElement(name="mensaje-error")
	/**
	 * Mensaje de error retornado por el modulo antifraudes.
	 */
	private String mensajeError;
	/**
	 * Objeto con la respuesta retornada por el maf.
	 */
	@XmlElement(name="respuesta-maf-prevalidacion")
	private RespuestaMafPrevalidacionComplexType respuestaMaf;
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(int codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public int getCodigoError() {
		return codigoError;
	}
	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public RespuestaMafPrevalidacionComplexType getRespuestaMaf() {
		return respuestaMaf;
	}
	public void setRespuestaMaf(RespuestaMafPrevalidacionComplexType respuestaMaf) {
		this.respuestaMaf = respuestaMaf;
	}

}

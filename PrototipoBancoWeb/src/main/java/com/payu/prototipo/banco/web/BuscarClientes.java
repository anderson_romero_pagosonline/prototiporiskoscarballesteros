package com.payu.prototipo.banco.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigation;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.repeater.IItemReuseStrategy;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;





import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.payu.prototipo.banco.persistencia.Cliente;
import com.payu.prototipo.banco.web.negocio.IGestorNegocio;

/**
 * Clase encargada de implementar los componentes correspondientes a la pagina para Listar, y eliminar los cliente.
 * @author <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
public class BuscarClientes extends Navigation {

	/**
	 * Id de serializacion de la clase.
	 */
	private static final long serialVersionUID = -696485953756295769L;

	/**
	 * Objeto logger para la realizacion del seguimiento de log en la clase.
	 */
 	private static final Logger logger = Logger.getLogger(BuscarClientes.class.getName());
	
	/**
	 * Listado de clientes a presentar el tabla de clientes.
	 */
	private List<Cliente> clientes = new ArrayList<>();
	
	/**
	 * Interfaz al objeto de negocio spring que implementa la logica de interaccion con gigaspaces.
	 */
	@SpringBean
	private IGestorNegocio negocio;
	/**
	 * Listado de modelo de personas a presentar los clientes consultados.
	 */
	final List<IModel<Cliente>> persons = new ArrayList<IModel<Cliente>>();
	
	/**
	 * Constructor de la pagina de busqueda y edicion de clientes, configura la presentacion.
	 */
	public BuscarClientes() {
		
		cargarClientes();
			
		
		ListDataProvider<Cliente> listDataProviederCliente = new ListDataProvider<Cliente>(clientes);
		DataView<Cliente> dataViewClientes = new DataView<Cliente>("rows",listDataProviederCliente) {

			/**
			 * Metodo encargado de construir la presentacion con la tabla de clientes
			 */
			@Override
			protected void populateItem(Item<Cliente> item) {
				Cliente cliente = item.getModelObject();
				RepeatingView repiting = new RepeatingView("dataRow");
				repiting.add(new Label(repiting.newChildId(), cliente.getNombre()));
				repiting.add(new Label(repiting.newChildId(), cliente.getDireccion()));
				repiting.add(new Label(repiting.newChildId(), cliente.getTelefono()));
				repiting.add(new Label(repiting.newChildId(), cliente.getEstado()));

				Link<Cliente> linkEliminar = new Link<Cliente>("linkEliminar", item.getModel()) {
					@Override
					public void onClick() {
						logger.fine("Eliminando Cliente "+ getModelObject());
						negocio.eliminarCliente(getModelObject());
						cargarClientes();
						success("Cliente eliminado exitosamente");
						
					}
					
				};
				
				Link<Cliente> linkEditar = new Link<Cliente>("linkEditar", item.getModel()) {
					/**
					 * Implementacion de logica a ejecutar cuando se pulsa el boton editar
					 */
					@Override
					public void onClick() {
						PageParameters parameters = new PageParameters();
						parameters.set(0, getModelObject().getId());
						setResponsePage(CrearClientes.class, parameters);
					}
				};
				
				item.add(linkEliminar);
				item.add(linkEditar);
				
				item.add(repiting);
			}		
			
		};
		
		dataViewClientes.setItemsPerPage(10);
		add(dataViewClientes);
		add(new PagingNavigation("paginador", dataViewClientes));
		
	}
	
	/**
	 * Metodo encargado de consultar los clientes a presentar en la pagina.
	 */
	private void cargarClientes() {
		List<Cliente> clientesNu = negocio.cargarClientes();
		clientes.clear();
		clientes.addAll(clientesNu);
		logger.info("Clientes Cargados  " + clientes.size());
	}
		

	public void setNegocio(IGestorNegocio negocio) {
		this.negocio = negocio;
	}

}

package com.payu.prototipo.banco.web;

import org.apache.wicket.markup.html.link.Link;

/**
 * Clase que implementa la pagina con las opciones para la administrar clientes.
 * @author <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
public class Clientes extends Navigation {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor de la clase, se encarga de configurar la pagina con la navegacion a las opciones.
	 */
	public Clientes() {
		super();
		Link link = new Link("crearClientes") {
			@Override
			public void onClick() {
				setResponsePage(CrearClientes.class);
				
			}
		};
		add(link);
		
		Link linkBuscar = new Link("buscarClientes") {
			@Override
			public void onClick() {
				setResponsePage(BuscarClientes.class);
				
			}
		};
		add(linkBuscar);

    }
}

package com.payu.prototipo.banco.web;

import org.apache.wicket.markup.html.link.Link;

/**
 * Clase encargada de configurar la presentacion de la pagina con el menu de opciones de cuentas.
 * @author <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
public class Cuentas extends Navigation {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor de la clase, se encarga de configurar la navegacion hacia las opciones de cuentas.
	 */
	public Cuentas() {
		super();
		Link link = new Link("crearCuentas") {
			@Override
			public void onClick() {
				setResponsePage(CrearCuentas.class);
				
			}
		};
		add(link);
		
		Link linkBuscar = new Link("buscarCuentas") {
			@Override
			public void onClick() {
				setResponsePage(BuscarCuentas.class);
				
			}
		};
		add(linkBuscar);

    }
}

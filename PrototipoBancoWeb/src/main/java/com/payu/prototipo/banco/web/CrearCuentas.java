package com.payu.prototipo.banco.web;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.RangeValidator;

import com.payu.prototipo.banco.persistencia.Cliente;
import com.payu.prototipo.banco.persistencia.Cuenta;
import com.payu.prototipo.banco.web.negocio.IGestorNegocio;

/**
 * Clase encargada de configurar e implementar la presentacion para la pagina encargada de la creacion de cuentas.
 * @author <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
public class CrearCuentas extends Navigation {
	/**
	 * Identificador de serializacion de clase. 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Objeto logger para la realizacion del seguimiento de log en la clase.
	 */
	private static final Logger logger = Logger.getLogger(CrearCuentas.class.getName());
	
	/**
	 * Objeto cuenta el cual se creara.
	 */
	private Cuenta cuenta;
	/**
	 * Formulario que configura la recoleccion de informacion de la cuenta.
	 */
	private Form<Cuenta> formulario;
	
	/**
	 * Listado clientes para los que se puede realizar la creacion de cuentas.
	 */
	private List<Cliente> clientes = new ArrayList<>();
	
	/**
	 * Interfaz al objeto de negocio spring que implementa la logica de interaccion con gigaspaces.
	 */
	@SpringBean
	private IGestorNegocio negocio;

	/**
	 * Constructor de la clase, encargado de configurar la presentacion para la pagina encargada de la creacion de cuentas.
	 */
	public CrearCuentas() {
		cuenta = new Cuenta();
		
		clientes = negocio.cargarClientes();
		logger.fine("Numero de Cliente " + clientes.size());
		
		CompoundPropertyModel<Cuenta> modeloCliente = new CompoundPropertyModel<Cuenta>(cuenta);
		
		formulario = new Form<Cuenta>("formaCrearCliente", modeloCliente) {
			/**
			 * Metodo encargado de implementar la funcionalidad del boton crear cuenta.
			 */
			@Override
			protected void onSubmit() {
				logger.fine("Submit Cuenta " + cuenta);
				negocio.crearCuenta(cuenta);
				success("Cuenta Creada Exitosamente");
				this.clearInput();
				cuenta = new Cuenta();
				CompoundPropertyModel<Cuenta> modeloCuenta = new CompoundPropertyModel<Cuenta>(cuenta);
				this.setModel(modeloCuenta);
			}
			
		};
		TextField fieldNumero = new TextField("numero");
		fieldNumero.setRequired(true);
		formulario.add(fieldNumero);
		
		TextField fieldSaldo = new TextField("saldo");
		fieldSaldo.setType(Double.class);
		fieldSaldo.add(RangeValidator.minimum(0.0));
		fieldSaldo.setRequired(true);
		formulario.add(fieldSaldo);
		
		ChoiceRenderer<Cliente> comboRender = new ChoiceRenderer<Cliente>("nombre");
		DropDownChoice<Cliente> comboClientes = new DropDownChoice<Cliente>("cliente", clientes, comboRender);
		comboClientes.setRequired(true);
		formulario.add(comboClientes);
		
		add(formulario);
		
    }

	public void setNegocio(IGestorNegocio negocio) {
		this.negocio = negocio;
	}
}

package com.payu.prototipo.banco.web.negocio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import org.openspaces.core.GigaSpace;
import org.openspaces.core.GigaSpaceConfigurer;
import org.openspaces.core.space.UrlSpaceConfigurer;
import org.springframework.stereotype.Service;

import com.j_spaces.core.IJSpace;
import com.j_spaces.core.client.SQLQuery;
import com.payu.prototipo.banco.persistencia.Cliente;
import com.payu.prototipo.banco.persistencia.Cuenta;
import com.payu.prototipo.banco.persistencia.Movimiento;
import com.payu.prototipo.banco.persistencia.TipoMovimiento;

/**
 * Clase encargada de implementar la logica de negocio para la interacion con el espacio creado en gigaspaces
 * su gestion es realizada por spring.
 * @author oscar.ballesteros <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
@Service("GestorNegocio")
public class GestorNegocioImp implements IGestorNegocio {
	
	/**
	 * Clase que implementa el log de la clase.
	 */
	private static Logger logger = Logger.getLogger(GestorNegocioImp.class.getName());

	/**
	 * Representacion del api para la comunicacion con el espacio de gigaspaces.
	 */
	private GigaSpace gigaSpace;
	
	
	/**
	 * Constructor sin parametros para el objeto de negocio, se encarga de configurar el objeto para interactuar con el espacio.
	 */
	public GestorNegocioImp() {
		String url = "jini://*/*/space";
		IJSpace space = new UrlSpaceConfigurer(url).space();
		gigaSpace = new GigaSpaceConfigurer(space).gigaSpace();
	}

	/**
	 * @see IGestorNegocio#buscarCliente(String)
	 */
	@Override
	public Cliente buscarCliente(String id) {
		return gigaSpace.readById(Cliente.class, id);
	}
	
	/**
	 * @see IGestorNegocio#crearCliente(Cliente)
	 */
	@Override
	public void crearCliente(Cliente cliente) {
		logger.info("Creando Cliente "+ cliente);
		
		cliente.setEstado(Cliente.ESTADO_ACTIVO);
		gigaSpace.write(cliente);
		logger.fine("Cliente Creado" + cliente);

	}

	/**
	 * @see IGestorNegocio#cargarClientes()
	 */
	@Override
	public List<Cliente> cargarClientes() {
		logger.info("Listando Clientes");
		SQLQuery<Cliente> queryClientes = new SQLQuery<Cliente>(Cliente.class,"(estado = ?)");
		queryClientes.setParameter(1, Cliente.ESTADO_ACTIVO);
		
		Cliente[] clientes = gigaSpace.readMultiple(queryClientes);
		logger.finer("Numero de resultados " + clientes.length);
		
		List<Cliente> listaClientes = Arrays.asList(clientes);
		return listaClientes;
	}

	/**
	 * @see IGestorNegocio#eliminarCliente(Cliente)
	 */
	@Override
	public void eliminarCliente(Cliente cliente) {
		logger.info("Eliminando Clientes");
		cliente.setEstado(Cliente.ESTADO_ELIMINADO);
		gigaSpace.write(cliente);
	}

	/**
	 * @see IGestorNegocio#crearCuenta(Cuenta)
	 */
	@Override
	public void crearCuenta(Cuenta cuenta) {
		logger.info("Creando cuenta " + cuenta);
		Cliente cliente = buscarCliente(cuenta.getCliente().getId());
		cliente.getCuentas().add(cuenta);
		cuenta.setEstado(Cuenta.ESTADO_CUENTA_ACTIVO);
		gigaSpace.write(cliente);
		gigaSpace.write(cuenta);
		
		
	}

	/**
	 * @see IGestorNegocio#buscarCuentas(Cliente)
	 */
	@Override
	public List<Cuenta> buscarCuentas(Cliente cliente) {
		logger.info("Buscando cuentas para el cliente " + cliente);
		SQLQuery<Cuenta> queryCuentas = new SQLQuery<Cuenta>(Cuenta.class, "(cliente.id = ? and estado = ?)");
		queryCuentas.setParameter(1, cliente.getId());
		queryCuentas.setParameter(2, Cuenta.ESTADO_CUENTA_ACTIVO);
		
		Cuenta[] cuentasConsulta = gigaSpace.readMultiple(queryCuentas);
		logger.fine("Numero de cuentas del cliente "+cuentasConsulta.length);
		
		List<Cuenta> cuentas = Arrays.asList(cuentasConsulta);
		
		return cuentas;
	}

	/**
	 * @see IGestorNegocio#eliminarCuenta(Cuenta)
	 */
	@Override
	public void eliminarCuenta(Cuenta cuenta) {
		logger.info("Eliminando cuenta "+cuenta);
		Cliente cliente = this.buscarCliente(cuenta.getCliente().getId());
		cliente.getCuentas().remove(cuenta);
		cliente.getCuentas().add(cuenta);	
		
		cuenta.setEstado(Cuenta.ESTADO_CUENTA_ELIMINADO);
		gigaSpace.write(cliente);
		gigaSpace.write(cuenta);
	}

	/**
	 * @see IGestorNegocio#buscarTiposMovimiento()
	 */
	@Override
	public List<TipoMovimiento> buscarTiposMovimiento() {
		TipoMovimiento[] tiposMovimiento = gigaSpace.readMultiple(new TipoMovimiento());
		logger.info("Tipos de movimientos cargados " + tiposMovimiento.length);
		return Arrays.asList(tiposMovimiento);
	}

	/**
	 * @see IGestorNegocio#realizarMovimiento(Movimiento)
	 */
	@Override
	public void realizarMovimiento(Movimiento movimiento) {
		logger.info("Realizando movimeinto para la cuenta " + movimiento.getCuenta().getNumero());
		movimiento.setEstado(Movimiento.ESTADO_PENDIENTE);
		movimiento.setFecha(new Date());
		movimiento.setId(movimiento.getCuenta().getNumero() + "_" + System.currentTimeMillis());
		
		gigaSpace.write(movimiento);
		
		Movimiento movimientoResp = gigaSpace.readById(Movimiento.class, movimiento.getId(), null, 5000l);
		
		logger.info("Estado del movimiento " + movimientoResp.getEstado());
		if(Movimiento.ESTADO_FONDOS_INSUFICIENTES.equals(movimientoResp.getEstado())) {
			throw new IllegalStateException("Fondos insuficientes para realizar el movimiento");
		} else if (Movimiento.ESTADO_PENDIENTE.equals(movimientoResp.getEstado())) {
			throw new IllegalStateException("Aun no se ha terminado de procesar el movimiento");
		}
		
	}

	/**
	 * @see IGestorNegocio#ejecutarReporteCuentas(Cliente, Date, Date)
	 */
	@Override
	public List<Cuenta> ejecutarReporteCuentas(Cliente cliente,Date fechaInicial,Date fechaFinal) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fechaFinal);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		
		fechaFinal = calendar.getTime();
		

		logger.info("Ejecutando reporte de cuentas para el cliente " + cliente + " Fecha inicial " + fechaInicial + " Fecha Final " + fechaFinal);
		SQLQuery<Movimiento> reporteQuery = new SQLQuery<Movimiento>(Movimiento.class, "(cuenta.cliente.id = ? and estado = ? and fecha BETWEEN ? and ?)");
		reporteQuery.setParameter(1, cliente.getId());
		reporteQuery.setParameter(2, Movimiento.ESTADO_ACEPTADO);
		reporteQuery.setParameter(3, fechaInicial);
		reporteQuery.setParameter(4, fechaFinal);
		
		Movimiento[] movimientos = gigaSpace.readMultiple(reporteQuery);
		
		logger.fine("Numero de movimientos consultados " + movimientos.length);
		
		List<Cuenta> cuentas = new ArrayList<>();
		
		for(Movimiento m : movimientos) {
			logger.finer("Cuenta " + m.getCuenta() + " movimiento " + m.getId() + " valor " + m.getValor());
			Cuenta cuenta = m.getCuenta();
			if(cuentas.contains(cuenta)) {
				int indice = cuentas.indexOf(cuenta);
				cuentas.get(indice).getMovimientos().add(m);
			} else {
				Cuenta c = new Cuenta(cuenta.getNumero(), null, cuenta.getSaldo());
				c.setMovimientos(new HashSet<Movimiento>());
				c.getMovimientos().add(m);
				cuentas.add(c);
			}
		}	
		logger.finer("Numero de cuentas agregadas " + cuentas.size());
		return cuentas;
	}

	

}

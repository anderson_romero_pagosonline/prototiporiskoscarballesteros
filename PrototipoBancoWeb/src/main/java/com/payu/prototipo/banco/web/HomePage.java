package com.payu.prototipo.banco.web;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.WebPage;

/**
 * Clase que implementa la pagina Home del aplicativo, con las opciones de navegacion.
 * @author <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
public class HomePage extends Navigation {
	/**
	 * Identificador de serializacion de la pagina.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor por defecto de la pagina
	 */
	public HomePage() {
		super();

    }
}

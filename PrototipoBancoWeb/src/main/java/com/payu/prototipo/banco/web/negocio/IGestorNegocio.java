package com.payu.prototipo.banco.web.negocio;

import java.util.Date;
import java.util.List;

import com.payu.prototipo.banco.persistencia.Cliente;
import com.payu.prototipo.banco.persistencia.Cuenta;
import com.payu.prototipo.banco.persistencia.Movimiento;
import com.payu.prototipo.banco.persistencia.TipoMovimiento;
/**
 * Interface representando la interfaz entre la aplicacion web y la aplicacion gigaspaces.
 * @author <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
public interface IGestorNegocio {
	
	/**
	 * Metodo encargado de encontrar un cliente por su identificador.
	 * @param id Identificador del cliente a buscar.
	 * @return Objeto cliente consultado.
	 */
	Cliente buscarCliente(String id);
	/**
	 * Metodo encargado de crear un nuevo cliente.
	 * @param cliente Cliente a crear.
	 */
	void crearCliente(Cliente cliente);
	/**
	 * metodo encargado de cargar todos los clientes almacenados en el espacio.
	 * @return Listado con todos los objetos cliente.
	 */
	List<Cliente> cargarClientes();
	/**
	 * Metodo encargado de eliminar un cliente en el espacio.
	 * @param cliente cliente a eliminar.
	 */
	void eliminarCliente(Cliente cliente);
	/**
	 * Metodo encargado de crear una cuenta asociada a un cliente.
	 * @param cuenta cuenta a crear. 
	 */
	void crearCuenta(Cuenta cuenta);
	/**
	 * Metodo encargado de buscar todas las cuentas pertenecientes a un cliente.
	 * @param cliente cliente para el que se consultan las cuentas.
	 * @return Listado de cuentas para un cliente.
	 */
	List<Cuenta> buscarCuentas(Cliente cliente);
	/**
	 * Metodo encargado de eliminar una cuenta.
	 * @param cuenta cuenta a eliminar.
	 */
	void eliminarCuenta(Cuenta cuenta);
	/**
	 * Metodo encargado de consultar el catalogo de tipos de movimiento.
	 * @return Listado de los tipos de movimientos soportados.
	 */
	List<TipoMovimiento> buscarTiposMovimiento();
	/**
	 * Metodo encargado de ejecutar un movimiento sobre una cuenta.
	 * @param movimiento Movimiento a ejecutar en una cuenta.
	 */
	void realizarMovimiento(Movimiento movimiento);
	/**
	 * Metodo encargado de ejecutar el reporte de movimientos para un cliente dada una fecha inicial y final.
	 * @param cliente cliente para quien se ejecuta el reporte.
	 * @param fechaInicial fecha inicial de ejecucion del reporte.
	 * @param fechaFinal Fecha final de ejecucion del reporte.
	 * @return Listado de cuentas con la iformación de los movimientos realizados en el periodo de tiempo.
	 */
	List<Cuenta> ejecutarReporteCuentas(Cliente cliente,Date fechaInicial,Date fechaFinal);
}

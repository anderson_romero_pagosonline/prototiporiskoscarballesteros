package com.payu.prototipo.banco.web;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
/**
 * Clase que implementa la plantilla de presentacion para el aplicativo.
 * @author <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
public class Navigation extends WebPage {

	/**
	 * Constructor sin parametros de la clase, configura los mensaje a mostrar en todas las paginas.
	 */
	public Navigation() {
		super();
		FeedbackPanel mensajes = new FeedbackPanel("mensajes");
		add(mensajes);
	}
	
	/**
	 * Constructor con parametros de pagina para la clase, configura los mensaje a mostrar en todas las paginas.
	 */
	public Navigation(final PageParameters parameters){
		super(parameters);
		FeedbackPanel mensajes = new FeedbackPanel("mensajes");
		add(mensajes);
	}

}

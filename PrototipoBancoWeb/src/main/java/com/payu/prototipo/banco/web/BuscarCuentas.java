package com.payu.prototipo.banco.web;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigation;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.payu.prototipo.banco.persistencia.Cliente;
import com.payu.prototipo.banco.persistencia.Cuenta;
import com.payu.prototipo.banco.web.negocio.IGestorNegocio;
/**
 * Clase encargada de implementar la funcionalidad de la pagina para buscar las cuentas de un cliente.
 * @author <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
public class BuscarCuentas extends Navigation {

	/**
	 * Id de serializacion de la clase.
	 */
	private static final long serialVersionUID = 8124552548400388739L;
	
	/**
	 * Objeto logger para la realizacion de seguimiento.
	 */
	private static final Logger logger = Logger.getLogger(BuscarCuentas.class.getName());
	
	/**
	 * listado de cuentas a mostrar en la pagina.
	 */
	private List<Cliente> clientes = new ArrayList<>();
	/**
	 * Listado de cuentas a mostrar en la pagina para un determinado cliente.
	 */
	private List<Cuenta> cuentas = new ArrayList<>();
	
	/**
	 * Interfaz al objeto de negocio spring que implementa la logica de interaccion con gigaspaces.
	 */
	@SpringBean
	private IGestorNegocio negocio;
	
	/**
	 * Constructor de la clase, se encarga de configurar la presentación de la pagina.
	 */
	public BuscarCuentas() {
		
		clientes = negocio.cargarClientes();
		
		Model<Cliente> modeloClientes = new Model<>();
		ChoiceRenderer<Cliente> renderClientes = new ChoiceRenderer<Cliente>("nombre");
		DropDownChoice<Cliente> comboClientes = new DropDownChoice<Cliente>("clientes", modeloClientes, clientes, renderClientes){
			/**
			 *  Metodo encargado de cargar las cuentas de un cliente, dependiendo de el cliente seleccionado.
			 *  
			 */
			@Override
			protected void onSelectionChanged(Cliente newSelection) {
				logger.finer("Cliente seleccionado " + newSelection);
				cargarCuentas(newSelection);
			}

			/**
			 * Metodo que indica que cuando el combo de clientes cambie, se debe enviar una solicitud al servidor.
			 */
			@Override
			protected boolean wantOnSelectionChangedNotifications() {
				return true;
			}
			
		};
		
		add(comboClientes);
		
		ListDataProvider<Cuenta> listDataProviederCuenta = new ListDataProvider<Cuenta>(cuentas);
		DataView<Cuenta> dataViewClientes = new DataView<Cuenta>("rows", listDataProviederCuenta) {

			/**
			 * Metodo encargado de cargar la informacion en la tabla de cuentas dependiendo del cliente seleccionado.
			 */
			@Override
			protected void populateItem(Item<Cuenta> item) {
				Cuenta cuenta = item.getModelObject();
				RepeatingView repiting = new RepeatingView("dataRow");
				repiting.add(new Label(repiting.newChildId(), cuenta.getNumero()));
				repiting.add(new Label(repiting.newChildId(), cuenta.getSaldo()));
				repiting.add(new Label(repiting.newChildId(), cuenta.getEstado()));
				

				Link<Cuenta> linkEliminar = new Link<Cuenta>("linkEliminar", item.getModel()) {
					/**
					 * Metodo que ejecuta la logica al oprimir el link para eliminar una cuenta.
					 */
					@Override
					public void onClick() {
						logger.fine("Eliminando Cuenta " + getModelObject());
						eliminarCuenta(getModelObject());
						this.getParent().getParent().success("Cuenta eliminada exitosamente");
						
					}
					
				};
												
				item.add(linkEliminar);				
				item.add(repiting);
			}		
			
		};
		
		dataViewClientes.setItemsPerPage(10);
		add(dataViewClientes);
		add(new PagingNavigation("paginador", dataViewClientes));
		
	
		
	}
	
	/**
	 * Metodo encargado de cargar las cuentas para un cliente.
	 * @param clienteSeleccionado cliente para el que se cargaran las cuentas.
	 */
	private void cargarCuentas(Cliente clienteSeleccionado) {
		this.cuentas.clear();
		List<Cuenta> cuentasCliente = negocio.buscarCuentas(clienteSeleccionado);
		this.cuentas.addAll(cuentasCliente);
		
	}
	
	/**
	 * Metodo encargado de eliminar la cuenta seleccionada.
	 * @param cuentaEliminar cuenta a eliminar.
	 */
	private void eliminarCuenta(Cuenta cuentaEliminar) {
		negocio.eliminarCuenta(cuentaEliminar);
		cargarCuentas(cuentaEliminar.getCliente());
	}
	

	public void setNegocio(IGestorNegocio negocio) {
		this.negocio = negocio;
	}

}

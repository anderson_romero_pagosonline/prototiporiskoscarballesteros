package com.payu.prototipo.banco.web;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.RangeValidator;

import com.payu.prototipo.banco.persistencia.Cliente;
import com.payu.prototipo.banco.persistencia.Cuenta;
import com.payu.prototipo.banco.persistencia.Movimiento;
import com.payu.prototipo.banco.persistencia.TipoMovimiento;
import com.payu.prototipo.banco.web.negocio.IGestorNegocio;

/**
 * Clase que implementa la presentacion y logica de la pagina encargada de realizar movimientos sobres las cuentas.
 * @author <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
public class Movimientos extends Navigation {

	/**
	 * Id de serializacion de la clase.
	 */
	private static final long serialVersionUID = 8550384046293405420L;
	
	/**
	 * Objeto logger para la realizacion del seguimiento de log en la clase.
	 */
	private static final Logger logger = Logger.getLogger(Movimientos.class.getName());
	
	/**
	 * Listado de clientes a seleccionar para quien se realizara el movimiento.
	 */
	private List<Cliente> clientes = new ArrayList<>();
	/**
	 * Listado de cuentas pertenecientes al cliente seleccionado.
	 */
	private List<Cuenta> cuentas = new ArrayList<>();
	/**
	 * Listado con los tipos de movimiento que se pueden realizar en el sistema.
	 */
	private List<TipoMovimiento> tiposMovimiento = new ArrayList<TipoMovimiento>();
	/**
	 * Objeto movimiento a procesar.
	 */
	private Movimiento movimiento;
	
	/**
	 * Objeto wicket representando el combo en el cual se seleccionan las cuentas.
	 */
	private DropDownChoice<Cuenta> comboCuentas;
	
	/**
	 *Formulario a utilizar para capturar la informacion del movimiento a ejecutar.
	 */
	private Form<Movimiento> formulario;
	
	/**
	 * Interfaz al objeto de negocio spring que implementa la logica de interaccion con gigaspaces.
	 */
	@SpringBean
	private IGestorNegocio negocio;
	
	/**
	 * Constructor de la clase, se encarga de configurar la presentacion para la realizacion de movimientos.
	 */
	public Movimientos() {
		movimiento = new Movimiento();
		clientes = negocio.cargarClientes();
		cuentas = new ArrayList<>();
		tiposMovimiento = negocio.buscarTiposMovimiento();
		
		CompoundPropertyModel<Movimiento> modeloMovimiento = new CompoundPropertyModel<>(movimiento);
		formulario = new Form<Movimiento>("formaMovimientos", modeloMovimiento) { 

			/**
			 * Metodo encargado de procesar la informacion enviada desde el formulario de movimientos.
			 */
			@Override
			protected void onSubmit() {
				logger.info("Realizando movimiento " + movimiento.getValor() + " tipo " + movimiento.getTipoMovimiento().getDescripcion());
				try{
					negocio.realizarMovimiento(movimiento);
					success("Movimiento realizado exitosamente");
				}catch(IllegalStateException e) {
					logger.log(Level.WARNING, e.getMessage(), e);
					error("Error al realizar el movimiento, " + e.getMessage());
				}
				
				movimiento = new Movimiento();
				CompoundPropertyModel<Movimiento> modeloMovimiento = new CompoundPropertyModel<>(movimiento);
				this.setModel(modeloMovimiento);
			}
			
		};
		
		Model<Cliente> modeloClientes = new Model<>();
		ChoiceRenderer<Cliente> renderClientes = new ChoiceRenderer<Cliente>("nombre");
		DropDownChoice<Cliente> comboClientes = new DropDownChoice<Cliente>("clientes", modeloClientes, clientes, renderClientes){

			/**
			 * Metodo encargado de ejecutar la logica cuando se cambia la seleccion en el combo de clientes.
			 */
			@Override
			protected void onSelectionChanged(Cliente newSelection) {
				logger.fine("Cliente Seleccionado " + newSelection);
				cargarCuentas(newSelection);
			}
			/**
			 * Metodo que indica que cuando se cambie el valor en el combo de clientes se envia una solicitud al servidor.
			 */
			@Override
			protected boolean wantOnSelectionChangedNotifications() {
				return true;
			}
			
		};
		comboClientes.setRequired(true);
		
		formulario.add(comboClientes);				
		
		ChoiceRenderer<Cuenta> renderCuentas = new ChoiceRenderer<Cuenta>("numero");
		comboCuentas = new DropDownChoice<Cuenta>("cuenta", cuentas, renderCuentas);
		comboCuentas.setRequired(true);
		formulario.add(comboCuentas);
		
		ChoiceRenderer<TipoMovimiento> renderTipoMovimiento = new ChoiceRenderer<TipoMovimiento>("descripcion");
		DropDownChoice<TipoMovimiento> comboTipos = new DropDownChoice<TipoMovimiento>("tipoMovimiento", tiposMovimiento, renderTipoMovimiento);
		comboTipos.setRequired(true);
		formulario.add(comboTipos);
		
		TextField fieldValor = new TextField("valor");
		fieldValor.setRequired(true);
		fieldValor.setType(Double.class);
		fieldValor.add(RangeValidator.minimum(0.0));
		formulario.add(fieldValor);
		
		add(formulario);
		
	}
	
	/**
	 * Metodo encargado de cargar la cuentas de un cliente.
	 * @param clienteSeleccionado cliente seleccionado en el combo de clientes.
	 */
	private void cargarCuentas(Cliente clienteSeleccionado) {
		this.cuentas = negocio.buscarCuentas(clienteSeleccionado);
		logger.fine("Cuentas Seleccionadas " + clienteSeleccionado);
		comboCuentas.setChoices(cuentas);
		formulario.add(comboCuentas);
	}
	

	public void setNegocio(IGestorNegocio negocio) {
		this.negocio = negocio;
	}

}

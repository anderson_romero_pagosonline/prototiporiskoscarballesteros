package com.payu.prototipo.banco.web;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigation;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.googlecode.wicket.jquery.core.Options;
import com.googlecode.wicket.jquery.ui.form.datepicker.DatePicker;
import com.payu.prototipo.banco.persistencia.Cliente;
import com.payu.prototipo.banco.persistencia.Cuenta;
import com.payu.prototipo.banco.persistencia.Movimiento;
import com.payu.prototipo.banco.persistencia.TipoMovimiento;
import com.payu.prototipo.banco.web.negocio.IGestorNegocio;

/**
 * Clase encargada de implementar y configurar la presentacion para la pagina de reportes.
 * @author <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
public class Reportes extends Navigation {

	/**
	 * Id de serializacion de la clase.
	 */
	private static final long serialVersionUID = 8206460587975413657L;
	
	/**
	 * Objeto que representa la funcionalidad de rastreo de la clase.
	 */
	private static final Logger logger = Logger.getLogger(Reportes.class.getName());

	/**
	 * Listado de clientes para e cual se seleccionara uno.
	 */
	private List<Cliente> clientes = new ArrayList<>();
	
	/**
	 * Listado de cuentas pertenecientes a un cliete.
	 */
	private List<Cuenta> cuentas = new ArrayList<>();
	
	/**
	 * Objeto que representa al cliente seleccionado, al cual se le cargan las cuentas.
	 */
	private Cliente clienteSeleccionado;
	/**
	 * Fecha inicial del intervalo de tiempo de consulta.
	 */
	private Date fechaInicial;
	
	/**
	 * Fecha final del intervalo de tiempo de consulta.
	 */
	private Date fechaFin;
		
	/**
	 * Formulario en que se captura la informacion para la ejecucion del reporte.
	 */
	private Form<Reportes> formulario;
	
	/**
	 * Interfaz al objeto de negocio spring que implementa la logica de interaccion con gigaspaces.
	 */
	@SpringBean
	private IGestorNegocio negocio;
	
	/**
	 * Constructor de la clase, se encarga de configurar la presentacion para la pagina de reportes.
	 */
	public Reportes() {
		
		clientes = negocio.cargarClientes();
			
		CompoundPropertyModel<Reportes> modeloReportes = new CompoundPropertyModel<>(this);
		formulario = new Form<Reportes>("formaReportes", modeloReportes) {

			/**
			 * Metodo encargado de procesar la informacion enviada desde la forma de captura de datos del reporte
			 */
			@Override
			protected void onSubmit() {
				 logger.info("Reporte para cliente " + clienteSeleccionado + " fecha inicial: " + 
								    fechaInicial + " fecha fin: " + fechaFin);	
				if(fechaFin.before(fechaInicial)) {
					error("La Fecha Inicial es mayor a la Fecha final");
					return;
				}
				
				cargarReporteCuentas();
				
			}
			
		};
		
		ChoiceRenderer<Cliente> renderClientes = new ChoiceRenderer<Cliente>("nombre");
		DropDownChoice<Cliente> comboClientes = new DropDownChoice<Cliente>("clienteSeleccionado", clientes, renderClientes);
		comboClientes.setRequired(true);
		
		formulario.add(comboClientes);
		
		Options opciones = new Options("dateFormat", Options.asString("dd/mm/yy"));
		
		DatePicker pickerFechaInicial = new DatePicker("fechaInicial", opciones);
		pickerFechaInicial.setRequired(true);
		
		formulario.add(pickerFechaInicial);
		
		DatePicker pickerFechaFinal = new DatePicker("fechaFin", opciones);
		pickerFechaFinal.setRequired(true);
		
		formulario.add(pickerFechaFinal);
		
		add(formulario);
		
		
		ListDataProvider<Cuenta> listDataProviederCuenta = new ListDataProvider<Cuenta>(cuentas);
		DataView<Cuenta> dataViewClientes = new DataView<Cuenta>("rows", listDataProviederCuenta) {
			/**
			 * Metodo encargado de llenar la informacion de la tabla de resultados del reporte
			 */
			@Override
			protected void populateItem(Item<Cuenta> item) {
				Cuenta cuenta = item.getModelObject();
				RepeatingView repiting = new RepeatingView("dataRow");
				repiting.add(new Label(repiting.newChildId(), cuenta.getNumero()));
				repiting.add(new Label(repiting.newChildId(), cuenta.getSaldo()));
				BigDecimal debitos = BigDecimal.ZERO;
				BigDecimal creditos = BigDecimal.ZERO;
				logger.info("Numero de movimientos  " + cuenta.getMovimientos().size());
				for(Movimiento m : cuenta.getMovimientos()) {
					logger.info("Valor " + m.getValor());
					if(m.getTipoMovimiento().getId().equals(TipoMovimiento.ID_TIPO_MOVIMIENTO_CREDITO)) {				
						creditos = creditos.add(m.getValor());
					} else {
						debitos = debitos.add(m.getValor());
					}
				}
				repiting.add(new Label(repiting.newChildId(), debitos));
				repiting.add(new Label(repiting.newChildId(), creditos));
				item.add(repiting);
			}
			
		};
		
		dataViewClientes.setItemsPerPage(10);
		add(dataViewClientes);
		add(new PagingNavigation("paginador", dataViewClientes));
		
	}
	
	/**
	 * Metodo encargado de cargar y procesar la informacion en pantalla del reporte de movimientos.
	 */
	private void cargarReporteCuentas() {
		List<Cuenta> cuentasConsultadas = negocio.ejecutarReporteCuentas(clienteSeleccionado, fechaInicial, fechaFin);
		logger.fine("Numero de cuentas " + cuentasConsultadas.size());
		this.cuentas.clear();
		this.cuentas.addAll(cuentasConsultadas);
		fechaInicial = null;
		fechaFin = null;
		CompoundPropertyModel<Reportes> modeloReportes = new CompoundPropertyModel<>(this);
		formulario.setModel(modeloReportes);
	}
	
	

	public void setNegocio(IGestorNegocio negocio) {
		this.negocio = negocio;
	}

}

package com.payu.prototipo.banco.web;

import java.util.logging.Logger;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.EmailAddressValidator;
import org.apache.wicket.validation.validator.RangeValidator;

import com.payu.prototipo.banco.persistencia.Cliente;
import com.payu.prototipo.banco.web.negocio.IGestorNegocio;

/**
 * Clase encargada de implementar la logica y configurar la pagina para la creacion, edicion de clientes.
 * @author <a href='mailto:oscar.ballesteros@payulatam.com'>Oscar Ballesteros</a>
 *
 */
public class CrearClientes extends Navigation {
	/**
	 * Id de serializacion de la clase.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Objeto logger para la realizacion del seguimiento de log en la clase.
	 */
	private static final Logger logger = Logger.getLogger(CrearClientes.class.getName());
	
	/**
	 * Objeto cliente para el cual se almacena la informacion del mismo.
	 */
	Cliente cliente = new Cliente();
	/**
	 * Formulario wicket para la captura de datos del cliente.
	 */
	private Form<Cliente> formulario;
	
	/**
	 * Interfaz al objeto de negocio spring que implementa la logica de interaccion con gigaspaces.
	 */
	@SpringBean
	private IGestorNegocio negocio;

	/**
	 * Constructor de la clase, se encarga de configurar la presentacion para la pagina de creacion de cliente.
	 */
	public CrearClientes() {		
		
		CompoundPropertyModel<Cliente> modeloCliente = new CompoundPropertyModel<Cliente>(cliente);
		
		formulario = new Form<Cliente>("formaCrearCliente",modeloCliente){

			/**
			 * Metodo encargado de capturar la informacion del cliente y delegar la creación al objeto de negocio.
			 */
			@Override
			protected void onSubmit() {
				logger.fine("Submit persona " + cliente);
				negocio.crearCliente(cliente);
				success("Cliente Creado Exitosamente");
				this.clearInput();
				cliente = new Cliente();
				CompoundPropertyModel<Cliente> modeloCliente = new CompoundPropertyModel<Cliente>(cliente);
				this.setModel(modeloCliente);
			}
			
		};
		TextField fieldEntrada = new TextField("nombre");
		fieldEntrada.setRequired(true);
		formulario.add(fieldEntrada);
		
		TextField fieldCorreo = new TextField("direccion");
		fieldCorreo.add(EmailAddressValidator.getInstance());
		fieldCorreo.setRequired(true);
		formulario.add(fieldCorreo);
		
		TextField fieldTelefono = new TextField("telefono");
		fieldTelefono.setType(Integer.class);
		fieldTelefono.add(RangeValidator.minimum(0));
		fieldTelefono.setRequired(true);
		formulario.add(fieldTelefono);
		
		add(formulario);
		

    }
	
	/**
	 * Constructor con parametros para la clase, se encarga de configurar los componentes de presentaciíon para la edicion de clientes.
	 * @param parameters parametros en los que se envia el id del cliente a editar.
	 */
	public CrearClientes(final PageParameters parameters) {
		this();
		String idCliente = parameters.get(0).toString();
		logger.fine("id  de Cliente " + idCliente);
		this.cliente = negocio.buscarCliente(idCliente);
		
		CompoundPropertyModel<Cliente> modeloCliente = new CompoundPropertyModel<Cliente>(cliente);
		formulario.setModel(modeloCliente);
	}

	public void setNegocio(IGestorNegocio negocio) {
		this.negocio = negocio;
	}
}
